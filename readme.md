# File Download Table

## Intro

This project has been developed in [StencilJS](https://stenciljs.com/). The aim was to develop an example of a front-end project using a framework that others may be less familiar with.

The project builds native web components, which is an exciting, relatively new, advancement within the front-end development space. One of the benefits is the ability to view and manipulate components directly within the DOM when viewed in dev tools.

However, there are drawbacks, such-as fewer quantities of information available online, and the development tooling isn't as mature as other libraries, like React.

## Developer setup

After cloning the project.

```bash
cd file-download-table
npm i
npm start
```

This will open the dev server in the browser.

You can build the app with `npm run build`.

## Live editing components in the browser

It's possible to directly edit props from the console tab dev tools.

First store your elements as variables:

```javascript
const fileDownloadTable = document.querySelector('app-file-download-table');

// Note that `app-table` is located in the shadow DOM of `app-file-download-table` so the selector is a little different!
const tableEl = fileDownloadTable.shadowRoot.querySelector('app-table');
```

Toggle the tables ability to select / de-select rows:

```javascript
tableEl.shouldPreventRowSelection = true;

tableEl.shouldPreventRowSelection = false;
```

Alter the data within the table, setting everything to have the `available` status:

```javascript
fileDownloadTable.files = [...fileDownloadTable.files.map(file => ({ ...file, status: 'available' }))];
```

Re-write the data within the table entirely:

```javascript
fileDownloadTable.files = [
  { name: 'mspaint.exe', device: 'Foo', path: '\\Device\\HarddiskVolume1\\Windows\\System32\\mspaint.exe', status: 'available' },
  { name: 'pla.dll', device: 'Bar', path: '\\Device\\HarddiskVolume1\\Windows\\System32\\pla.dll', status: 'scheduled' },
  { name: 'ping.exe', device: 'Baz', path: '\\Device\\HarddiskVolume1\\Windows\\System32\\ping.exe', status: 'available' }
];
```

## Testing

I've included some test examples:

`src/components/button/test/button.e2e.ts` - checks the click event fires correctly.

`src/components/file-download-table/test/file-download-table.utils.spec.tsx` - checks the function returns the expected output with a given input.
