import { Component, Host, h } from '@stencil/core';

@Component({
  tag: 'app-status-indicator',
  styleUrl: 'status-indicator.css',
  shadow: true,
})
export class AppStatusIndicator {
  render() {
    return (
      <Host>
        <slot></slot>
      </Host>
    );
  }
}
