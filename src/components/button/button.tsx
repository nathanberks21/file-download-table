import { Component, h, Prop, Event, EventEmitter } from '@stencil/core';

@Component({
  tag: 'app-button',
  styleUrl: 'button.css',
  shadow: true,
})
export class AppButton {
  @Prop() isDisabled?: boolean;
  @Prop() buttonIconSrc?: string;
  @Prop() buttonIconAlt?: string;

  @Event() buttonClick: EventEmitter;

  handleClickOrKeyUp(): void {
    if (this.isDisabled) {
      return;
    }

    this.buttonClick.emit();
  }

  render() {
    return (
      <button
        disabled={this.isDisabled}
        onClick={() => this.handleClickOrKeyUp()}
        onKeyUp={ev => ev.key === 'Enter' && this.handleClickOrKeyUp()}
      >
        {!!this.buttonIconSrc && <img src={this.buttonIconSrc} alt={this.buttonIconAlt} />}
        <slot />
      </button>
    );
  }
}
