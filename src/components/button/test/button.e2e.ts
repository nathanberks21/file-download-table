import { newE2EPage } from '@stencil/core/testing';

describe('app-button', () => {
  it('handles click events', async () => {
    const page = await newE2EPage();
    await page.setContent('<app-button />');

    const buttonEl = await page.find('app-button >>> button');

    const buttonClickEventPromise = page.waitForEvent('buttonClick');

    buttonEl.click();

    const buttonClickEvent = await buttonClickEventPromise;

    expect(buttonClickEvent).toBeDefined();
  });
});
