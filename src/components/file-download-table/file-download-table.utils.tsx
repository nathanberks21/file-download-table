import { h } from '@stencil/core';
import { File } from '../../components';
import { Cell, TableRowData } from '../../types/table.types';
import { FileStatus } from '../../types/file.types';

export function initTableRowData(files: File[]): Map<string, TableRowData> {
  const tableRowMapData = files.map<[string, TableRowData]>(file => [
    file.path,
    {
      cells: generateCellElements(file),
      isSelected: false,
      isDisabled: file.status === FileStatus.SCHEDULED,
    },
  ]);

  return new Map(tableRowMapData);
}

function generateCellElements({ name, device, path, status }): Cell[] {
  return [
    name,
    <app-device-profile deviceName={device} />,
    <app-path-indicator withStatusIndicator={status === FileStatus.AVAILABLE}>{path}</app-path-indicator>,
    getStatusText(status),
  ];
}

function getStatusText(status: FileStatus): string {
  switch (status) {
    case FileStatus.AVAILABLE:
      return 'Available';
    case FileStatus.SCHEDULED:
      return 'Scheduled';
  }
}
