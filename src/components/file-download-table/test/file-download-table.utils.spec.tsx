import { FileStatus } from '../../../types/file.types';
import { initTableRowData } from '../file-download-table.utils';

describe('app-file-download-table.utils', () => {
  describe('initTableRowData', () => {
    it('formats File data into TableRowData', () => {
      const mockFiles = [
        { name: 'a.exe', device: 'A', path: '\\foo\\bar\\a.exe', status: FileStatus.SCHEDULED },
        { name: 'b.exe', device: 'B', path: '\\foo\\bar\\baz\\b.exe', status: FileStatus.AVAILABLE },
      ];

      const result = initTableRowData(mockFiles);

      const firstRow = result.get('\\foo\\bar\\a.exe');
      const secondRow = result.get('\\foo\\bar\\baz\\b.exe');

      expect(firstRow.isDisabled).toEqual(true);
      expect(firstRow.isSelected).toEqual(false);
      expect(firstRow.cells[0]).toEqual('a.exe');
      expect(firstRow.cells[3]).toEqual('Scheduled');

      expect(secondRow.isDisabled).toEqual(false);
      expect(secondRow.isSelected).toEqual(false);
      expect(secondRow.cells[0]).toEqual('b.exe');
      expect(secondRow.cells[3]).toEqual('Available');
    });
  });
});
