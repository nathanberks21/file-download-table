import { Component, Prop, State, Watch, h } from '@stencil/core';
import { File } from '../../types/file.types';
import { TableRowData, TableRowSelectionEventData } from '../../types/table.types';
import { initTableRowData } from './file-download-table.utils';

@Component({
  tag: 'app-file-download-table',
  styleUrl: 'file-download-table.css',
  shadow: true,
})
export class AppFileDownloadTable {
  @Prop() files: File[];

  @State() tableData: Map<string, TableRowData>;

  get tableHeaders(): string[] {
    if (!this.files.length) {
      return [];
    }

    return Object.keys(this.files[0]);
  }

  @Watch('files')
  onFilesChanges() {
    this.tableData = initTableRowData(this.files);
  }

  componentWillLoad() {
    this.tableData = initTableRowData(this.files);
  }

  get selectedRows(): File[] {
    const selectedRowKeys = [...this.tableData.entries()]
      .filter(([, tableRowData]) => tableRowData.isSelected)
      .map(([key]) => key);

    return this.files.filter(({ path }) => selectedRowKeys.includes(path));
  }

  updateTableData({ rowIds, isSelected }: TableRowSelectionEventData): void {
    rowIds.forEach(rowId => this.tableData.set(rowId, { ...this.tableData.get(rowId), isSelected }));

    this.tableData = new Map(this.tableData);
  }

  displaySelectedRowsAlert(): void {
    const selectedRowsText = this.selectedRows.map(row => `${row.device}: ${row.path}`);

    alert(`You have selected:\n\n${selectedRowsText.join('\n')}`);
  }

  render() {
    if (!this.tableData) {
      return;
    }

    return (
      <app-table
        tableHeaders={this.tableHeaders}
        tableData={this.tableData}
        onRowSelectionChange={({ detail }) => this.updateTableData(detail)}
      >
        <app-button
          slot="extra-controls"
          isDisabled={!this.selectedRows.length}
          buttonIconSrc="assets/download.svg"
          buttonIconAlt="Download icon"
          onButtonClick={() => this.displaySelectedRowsAlert()}
        >
          Download Selected
        </app-button>
      </app-table>
    );
  }
}
