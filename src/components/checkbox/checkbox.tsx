import { Component, Event, EventEmitter, Host, h, Prop } from '@stencil/core';

@Component({
  tag: 'app-checkbox',
  styleUrl: 'checkbox.css',
  shadow: true,
})
export class AppCheckbox {
  @Prop() elementId: string;
  @Prop() isIndeterminate = false;
  @Prop() isChecked = false;
  @Prop() isDisabled = false;
  @Prop() labelText: string;
  @Prop() tabIndexOverride = 0;

  @Event() isCheckedChange: EventEmitter<boolean>;

  handleClickOrKeyUp() {
    this.isCheckedChange.emit(!(this.isChecked || this.isIndeterminate));
  }

  render() {
    return (
      <Host>
        <input
          type="checkbox"
          id={this.elementId}
          value={this.elementId}
          tabIndex={this.tabIndexOverride}
          indeterminate={this.isIndeterminate}
          checked={this.isChecked}
          disabled={this.isDisabled}
          onClick={ev => {
            ev.preventDefault();

            this.handleClickOrKeyUp();
          }}
          onKeyUp={ev => {
            if (ev.key === 'Enter') {
              this.handleClickOrKeyUp();
            }
          }}
        />
        {!!this.labelText && <label htmlFor={this.elementId}>{this.labelText}</label>}
      </Host>
    );
  }
}
