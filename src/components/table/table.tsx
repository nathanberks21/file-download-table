import { Component, h, Prop, Event, EventEmitter, Host } from '@stencil/core';
import { TableRowData, TableRowSelectionEventData } from '../../types/table.types';

@Component({
  tag: 'app-table',
  styleUrl: 'table.css',
  shadow: true,
})
export class AppTable {
  @Prop() tableHeaders: string[];
  @Prop() tableData: Map<string, TableRowData>;
  @Prop() shouldPreventRowSelection = false;

  @Event() rowSelectionChange: EventEmitter<TableRowSelectionEventData>;

  // Returns the counts of selected rows and the total number of selectable rows
  get numberOfRowsSelected(): { totalSelectable: number; totalSelected: number } {
    return [...this.tableData.values()].reduce(
      (counts, { isSelected, isDisabled }) => {
        if (isSelected) {
          counts.totalSelected++;
        }

        if (!isDisabled) {
          counts.totalSelectable++;
        }

        return counts;
      },
      { totalSelectable: 0, totalSelected: 0 },
    );
  }

  // Enable all selectable rows, or deselect if they all are already selected
  updateAllRowSelectionState(): void {
    const rowIdsToUpdate = [...this.tableData.entries()]
      .filter(([, { isDisabled }]) => !isDisabled)
      .map(([key]) => key);

    const { totalSelectable, totalSelected } = this.numberOfRowsSelected;
    const shouldSelectAllRows = totalSelected !== totalSelectable;

    this.rowSelectionChange.emit({ rowIds: rowIdsToUpdate, isSelected: shouldSelectAllRows });
  }

  render() {
    const { totalSelectable, totalSelected } = this.numberOfRowsSelected;
    const selectAllLabelText = totalSelected === 0 ? 'None Selected' : `Selected ${totalSelected}`;
    const areAllRowsSelected = totalSelected === totalSelectable;
    const areSomeRowsSelect = totalSelected > 0 && !areAllRowsSelected;

    return (
      <Host>
        <div class="table-controls">
          {!this.shouldPreventRowSelection && (
            <app-checkbox
              elementId="select-all-checkbox"
              labelText={selectAllLabelText}
              isChecked={areAllRowsSelected}
              isIndeterminate={areSomeRowsSelect}
              onIsCheckedChange={() => this.updateAllRowSelectionState()}
            />
          )}
          <slot name="extra-controls" />
        </div>
        <table>
          <thead>
            <tr>
              {/* Add an empty column if we want to render checkboxes for each row */}
              {!this.shouldPreventRowSelection && <th></th>}
              {this.tableHeaders.map(header => (
                <th>{header}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {[...this.tableData.entries()].map(([id, { isSelected, isDisabled, cells }]) => (
              <app-table-row
                key={id}
                rowId={id}
                isSelected={isSelected}
                isDisabled={isDisabled}
                cells={cells}
                shouldPreventRowSelection={this.shouldPreventRowSelection}
                onRowClickOrKeyUp={() => {
                  this.rowSelectionChange.emit({ rowIds: [id], isSelected: !isSelected });
                }}
              />
            ))}
          </tbody>
        </table>
      </Host>
    );
  }
}
