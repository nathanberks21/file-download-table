import { Component, Host, h, Prop } from '@stencil/core';

@Component({
  tag: 'app-path-indicator',
  styleUrl: 'path-indicator.css',
  shadow: true,
})
export class AppPathIndicator {
  @Prop() withStatusIndicator = false;

  render() {
    return (
      <Host>
        <slot />
        {this.withStatusIndicator && <div class="status-orb" />}
      </Host>
    );
  }
}
