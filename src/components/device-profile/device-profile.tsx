import { Component, Host, h, Prop } from '@stencil/core';

@Component({
  tag: 'app-device-profile',
  styleUrl: 'device-profile.css',
  shadow: true,
})
export class AppDeviceProfile {
  @Prop() deviceName: string;

  get deviceProfileImageSrc(): string | undefined {
    switch (this.deviceName) {
      case 'Luigi':
        return 'assets/luigi.png';
      case 'Mario':
        return 'assets/mario.png';
      case 'Peach':
        return 'assets/peach.png';
      case 'Daisy':
        return 'assets/daisy.png';
      case 'Yoshi':
        return 'assets/yoshi.png';
      case 'Toad':
        return 'assets/toad.png';
    }
  }

  render() {
    const deviceProfileImageSrc = this.deviceProfileImageSrc;

    return (
      <Host>
        {!!deviceProfileImageSrc && <img src={deviceProfileImageSrc} />}
        {this.deviceName}
      </Host>
    );
  }
}
