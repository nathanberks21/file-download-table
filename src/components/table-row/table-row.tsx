import { Component, Event, Prop, EventEmitter, h } from '@stencil/core';
import { Cell } from '../../types/table.types';

@Component({
  tag: 'app-table-row',
  styleUrl: 'table-row.css',
  shadow: true,
})
export class AppTableRow {
  @Prop() rowId: string;
  @Prop() shouldPreventRowSelection: boolean;
  @Prop() isSelected?: boolean;
  @Prop() isDisabled?: boolean;
  @Prop() cells: Cell[];

  @Event() rowClickOrKeyUp: EventEmitter<boolean>;

  get trSelectableAttributes(): { tabIndex?: 0; ariaSelected?: boolean; ariaDisabled?: boolean; ariaLabel?: string } {
    if (this.shouldPreventRowSelection) {
      return {};
    }

    const baseAttrs = {
      ariaSelected: this.isSelected,
      ariaLabel: `Select ${this.rowId}`,
      ariaDisabled: this.isDisabled,
    };

    if (this.isDisabled) {
      return baseAttrs;
    }

    return { ...baseAttrs, tabIndex: 0 };
  }

  handleClickOrKeyUp(): void {
    if (this.isDisabled || this.shouldPreventRowSelection) {
      return;
    }

    this.rowClickOrKeyUp.emit();
  }

  render() {
    return (
      <tr
        {...this.trSelectableAttributes}
        class={{
          selected: !this.shouldPreventRowSelection && this.isSelected,
          selectable: !this.shouldPreventRowSelection,
          disabled: this.isDisabled,
        }}
        onClick={() => this.handleClickOrKeyUp()}
        onKeyUp={ev => ev.key === 'Enter' && this.handleClickOrKeyUp()}
      >
        {!this.shouldPreventRowSelection && (
          <td>
            <app-checkbox tabIndex={-1} isChecked={this.isSelected} isDisabled={this.isDisabled} />
          </td>
        )}
        {this.cells.map((cell, idx) => (
          <td key={idx}>{cell}</td>
        ))}
      </tr>
    );
  }
}
