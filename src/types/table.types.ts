import { FunctionalComponent } from '@stencil/core';

export type Cell = (FunctionalComponent | string)[];

export type TableRowData = {
  cells: Cell[];
  isSelected?: boolean;
  isDisabled?: boolean;
};

export type TableRowSelectionEventData = { rowIds: string[]; isSelected: boolean };
